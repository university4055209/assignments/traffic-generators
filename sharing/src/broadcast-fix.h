#pragma once

#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <semaphore.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/select.h>

#define PORT 2299


typedef struct Payload {
    int value;
} Payload;

typedef struct Message {
    int source;
    int sequence;
    int payload_lenght;
    Payload payload;
} Message;

typedef struct knownHostNode {
    Message * contents;
    struct knownHostNode * next;
} knownHostNode;

//typedef void (*BroadcasterHandler)(Broadcaster * broadcaster, Payload * payload);

typedef struct Broadcaster {
    // .. Local data to manage broadcast
    // e.g. socket opened, network interfaces, 
    // sequence of other nodes...
    knownHostNode * nodes_list;
    int sending_socket; 
    int receiving_socket;
    struct sockaddr_in broadcast_addr;
    int addr_len;
    // struct sockaddr_in server_addr;
    //BroadcasterHandler handler;
    int id;
    int sequence_number;
} Broadcaster;

typedef struct TrafficAnalyzer {
    int count;
    int seconds;
    int elapsed;
    // Definition of sliding window...s
} TrafficAnalyzer;



Message * prepare_message(Payload * payload);
void bsend(Broadcaster * broadcaster, Message * i_msg, bool resending);
void bind_to_all_interfaces(int sock, void * context, void (*handler)(int, void *));
int msleep(long msec);
void * sender_f(void *);
bool checkList(Message * msg);
void listener(int socket, void * context);
void * receiver_f(void *);
void * traffic_f(void *);
