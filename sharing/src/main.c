#include "main.h"

// ===================== TRAFFIC GENERATOR ====================================
// The program works as follows: 
// when the main enters, three different threads are created to manage:
// 1. The sending of packets generated from the node itself every 10ms
// 2. The connection to the node from every interface. All the interfaces are
// checked one at a time to be sure that no message has arrived. After 3s
// the 'select()' exits and a new interface is scanned
// 3. The analytics. 
//
// Where the program does not seem to work:
// When running the program, no node seems to be able to detect the packets
// sent through the broadcast interface, even though by definition such packets
// should be sent via every interface available to the sender node. Due to this
// we extended the duration of the program to infinity (i.e. the main does not
// stop after 20 seconds). For the same reason, many frees/deallocations are 
// omitted. 
// ============================================================================

//Global variables: 

//Semaphore for the traffic analyzer
sem_t cs;
Broadcaster * broadcaster;
TrafficAnalyzer * traffic;
//fd_set used to read from interface
fd_set readfd;

//Creates a message given the payload
Message * prepare_message(Payload * payload){
    Message * msg = (Message *)calloc(1, sizeof(Message));
    msg->payload = *payload;
    msg->payload_lenght = sizeof(int);
    msg->sequence = broadcaster->sequence_number++;
    msg->source = broadcaster->id;
    return msg;
}

//Sends and create - if necessary - a broadcasted message. For each 
//successfully sent message, the throughput is updated
void bsend(Broadcaster * broadcaster, Payload * payload, Message * i_msg) {
    Message * msg = i_msg; 
    if(msg == NULL){ msg = prepare_message(payload); }
    // printf("Node:%d sending!\n", broadcaster->id);
    int ret = sendto(broadcaster->socket, msg, sizeof(Message), 0, (struct sockaddr *)&(broadcaster->broadcast_addr), broadcaster->addr_len);
    if(ret == -1){
        // printf("send error: errno: %d", errno);
        // perror("send error");
        return;
    }
    else{
        if(!sem_wait(&cs)){
            perror("wait error");
        }
        traffic->count++;
        // printf("traffic count: %d", traffic->count);
        if(!sem_post(&cs)){
            perror("post error");
        }
    }
    free(msg);
    free(payload);
    //refactor idea: the function could try to send the message until 
    //no more errors are returned
}

//-------------------------
// Utility
// ------------------------

/**
 * Bind the given socket to all interfaces (one by one)
 * and invoke the handler with same parameter
 */
void bind_to_all_interfaces(int sock, void * context, void (*handler)(int, void *)) {
    struct ifaddrs *addrs, *tmp;
    getifaddrs(&addrs);
    tmp = addrs;
    while (tmp){
        if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET) {
            setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, tmp->ifa_name, sizeof(tmp->ifa_name));
            handler(sock, context);
        }
        tmp = tmp->ifa_next;
    }
    freeifaddrs(addrs);
}

/**
 * Sleep a given amount of milliseconds
 */
int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do
    {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

//Routine function for the sender thread
void * sender_f(void * args){
    while(1){
        Payload * payload = (Payload *)calloc(1, sizeof(Payload));
        payload->value = rand()%100;
        bsend(broadcaster, payload, NULL);
        msleep(10);
    }
}

//Returns true if message was seen
bool checkList(Message * msg){
    if(broadcaster->nodes_list == NULL){
        broadcaster->nodes_list = (knownHostNode *)calloc(1, sizeof(knownHostNode));
        broadcaster->nodes_list->contents = (Message *)calloc(1, sizeof(Message));
        broadcaster->nodes_list->contents->payload = msg->payload;
        broadcaster->nodes_list->contents->payload_lenght = msg->payload_lenght;
        broadcaster->nodes_list->contents->sequence = msg->sequence;
        broadcaster->nodes_list->contents->source = msg->source;
        return false;
    }
    while(1){
        knownHostNode * temp = broadcaster->nodes_list;
        if(temp->contents->source == msg->source){
            bool ret = (temp->contents->sequence+1 == msg->sequence) ? false : true;
            if(!ret){
                temp->contents->sequence++;
            }
            return ret;
        }
        if(temp->next == NULL){
            temp->next = (knownHostNode *)calloc(1, sizeof(knownHostNode));
            temp->next->contents = (Message *)calloc(1, sizeof(Message));
            temp->next->contents->payload = msg->payload;
            temp->next->contents->payload_lenght = msg->payload_lenght;
            temp->next->contents->sequence = msg->sequence;
            temp->next->contents->source = msg->source;
            return false;
        }
    }

}

//(Auxiliary) routine function for the sender thread
void listener(int socket, void * context){
    Message * msg = (Message *)calloc(1, sizeof(Message));
    printf("Node:%d receiving!\n", broadcaster->id);

    FD_ZERO(&readfd);
    FD_SET(socket, &readfd);
    struct timeval time; 
    time.tv_sec = 3;
    time.tv_usec = 0;
    int ret = select(socket+1, &readfd, NULL, NULL, &time);
    if(ret>0){
        if(FD_ISSET(socket, &readfd)){
            printf("=== RECEIVED MESSAGE ===\n");
            ret = recvfrom(socket, msg, sizeof(Message), 0, NULL, NULL);
            if(ret == -1){
                printf("recv error: %d\n", errno);
                perror("receive error");
                return;
            }
            //Same refactor comment as in bsend
            if(msg->source == broadcaster->id){ return; }
            if(!checkList(msg)){
                bsend(broadcaster, &(msg->payload), msg);
            }
        }
    }
    printf("changing interface\n");
    //free(msg)
}

//Routine function for the sender thread
void * receiver_f(void * args){
    while(1){
        bind_to_all_interfaces(broadcaster->socket, NULL, listener);
    }
}

//Routine function for the analyzer thread
void * traffic_f(void * args){
    int denominator = 1;
    while(1){
        msleep(1000);
        // traffic->seconds++;
        // if(traffic->seconds != 1){
        //    denominator = 2;
        //}
        //if(traffic->seconds == 3)
        printf("%d: %f,", ++traffic->elapsed, (float)(traffic->count)/(float)(denominator));
        if(!sem_wait(&cs)){
            perror("wait error");
        }
        traffic->count = 0;
        if(!sem_post(&cs)){
            perror("post error");
        }
    }
}

int main(int argc, char * argv[]) {
    
    //Prepares stderr for docker (does not seem to work properly)
    setvbuf(stdout, NULL, _IONBF, 0);
    int ret; 

    //checks for arguments
    if(argc < 2){
        printf("Too few aruments\n");
        return -1;
    }
    int id = atoi(argv[1]);

    //initializes semaphore
    if(sem_init(&cs, 0, 1)==-1){
        perror("sem error");
        return -1;
    }

    //initializes traffic analyzer
    traffic = (TrafficAnalyzer *)calloc(1, sizeof(TrafficAnalyzer));
    
    //Creates socket and sets the broadcast option
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock < 0){
        perror("Sock error");
        return -1;
    }
    int yes = 1;
    ret = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char *)&yes, sizeof(yes));
    if(ret == -1){
        printf("broadcast setopt error");
    }
    
    //Prepares the broadcaster
    broadcaster = (Broadcaster *)calloc(1, sizeof(Broadcaster));
    broadcaster->nodes_list = (knownHostNode *)calloc(1,sizeof(knownHostNode));
    broadcaster->socket = sock;
    broadcaster->broadcast_addr.sin_family = AF_INET;
    broadcaster->broadcast_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    broadcaster->broadcast_addr.sin_port = PORT;
    broadcaster->addr_len = sizeof(struct sockaddr_in);
    broadcaster->id = id;
    broadcaster->sequence_number = 0;
    
    printf("Node %d: creating threads!\n", id);
    // Traffic generator
    pthread_t sender, receiver, analyzer;
    ret = pthread_create(&sender, NULL, sender_f, NULL);
    if(ret != 0){
        fprintf(stderr, "pthread error");
        return -1;
    }
    ret = pthread_create(&receiver, NULL, receiver_f, NULL); 
    if(ret != 0){
        fprintf(stderr, "pthread error");
        return -1;
    }
    ret = pthread_create(&analyzer, NULL, traffic_f, NULL);
    if(ret != 0){
        fprintf(stderr, "pthread error");
        return -1;
    }
    printf("Node %d: created threads!\n", id);

    while(1){}
    // msleep(20000);
    // frees, sem_destroy...
}
