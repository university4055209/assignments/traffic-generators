# Traffic Generators

Esercizio di programmazione in C, svolto da Pietrosanti Tommaso (1994391) e Zarola Massimo (1992617).  
Consegniamo il codice, per quanto non funzionante, nella speranza di poter ricevere un feedback su quanto possiamo aver sbagliato nella progettazione della soluzione. Lo scoglio oltre cui non siamo riusciti a proseguire, come da commenti al codice stesso, è la ricezione dei pacchetti.  
Ogni singolo nodo, tramite il suo thread apposito, effettua il controllo un'interfaccia alla volta alla ricerca di possibili pacchetti arrivati. Nessun pacchetto riesce mai a raggiungere la destinazione, per cui ci troviamo di fronte a tanti nodi che inviano a vuoto i loro pacchetti, che comunque vengono da quanto ci sembra creati e inviati.  
Altri dettagli implementativi sono presenti nei commenti al codice. Alleghiamo in più un esempio di output della nostra simulazione.
